package com.epam.ta10.bdd.po;

import com.epam.ta10.bdd.decorator.CustomFieldDecorator;
import com.epam.ta10.bdd.utils.DriverProvider;
import org.openqa.selenium.support.PageFactory;

abstract class AbstractPage {

  AbstractPage() {
    PageFactory.initElements(new CustomFieldDecorator(DriverProvider.getDriver()), this);
  }
}
