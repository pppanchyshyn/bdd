package com.epam.ta10.bdd.po;

import com.epam.ta10.bdd.decorator.elements.Button;
import com.epam.ta10.bdd.decorator.elements.TextInput;
import org.openqa.selenium.support.FindBy;

public class GmailPasswordPage extends AbstractPage {

  @FindBy(name = "password")
  private TextInput password;

  @FindBy(xpath = "//div[@id='passwordNext']//child::span[text()]")
  private Button next;

  public TextInput getPassword() {
    return password;
  }

  public Button getNext() {
    return next;
  }
}
