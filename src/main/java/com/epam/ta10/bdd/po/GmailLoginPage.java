package com.epam.ta10.bdd.po;

import com.epam.ta10.bdd.decorator.elements.Button;
import com.epam.ta10.bdd.decorator.elements.TextInput;
import com.epam.ta10.bdd.utils.DriverProvider;
import com.epam.ta10.bdd.utils.PropertyProvider;
import org.openqa.selenium.support.FindBy;

public class GmailLoginPage extends AbstractPage {

  @FindBy(id = "identifierId")
  private TextInput login;

  @FindBy(xpath = "//div[@id='identifierNext']//child::span[text()]")
  private Button next;

  public TextInput getLogin() {
    return login;
  }

  public Button getNext() {
    return next;
  }

  public void navigate() {
    DriverProvider.getDriver().get(PropertyProvider.PROPERTIES.getProperty("gmailLoinPage"));
  }
}
