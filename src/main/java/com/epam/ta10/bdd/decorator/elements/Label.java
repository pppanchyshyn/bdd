package com.epam.ta10.bdd.decorator.elements;

import org.openqa.selenium.WebElement;

public class Label extends Element {

  public Label(WebElement webElement) {
    super(webElement);
  }

  public String getText() {
    return webElement.getText();
  }

  public String getAttribute(String s){
    return webElement.getAttribute(s);
  }

  public String getLowerCaseTest() {
    return webElement.getText().toLowerCase();
  }

  public String cutInnerLabelText(Label innerLabel) {
    return webElement.getText().replace(innerLabel.getText(), "");
  }
}
