package com.epam.ta10.bdd.decorator.elements;

import com.epam.ta10.bdd.utils.DriverProvider;
import com.epam.ta10.bdd.utils.WaiterProvider;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

public class Button extends Element {

  public Button(WebElement webElement) {
    super(webElement);
  }

  public void click() {
    webElement.click();
  }

  public void clickAfterButtonBecomeClickable() {
    WaiterProvider.waitElementToBeClickable(webElement).click();
  }

  public void clickWithJS() {
    JavascriptExecutor executor = (JavascriptExecutor) DriverProvider.getDriver();
    executor.executeScript("arguments[0].click();", webElement);
  }
}
