package com.epam.ta10.bdd.decorator.elements;

import org.openqa.selenium.WebElement;

public class Element {

  WebElement webElement;

  Element(WebElement webElement) {
    this.webElement = webElement;
  }

  public WebElement getWebElement() {
    return webElement;
  }
}
