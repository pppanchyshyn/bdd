package com.epam.ta10.bdd.decorator.elements;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openqa.selenium.WebElement;

public class TextInput extends Element {

  public TextInput(WebElement webElement) {
    super(webElement);
  }

  public void sendKeys(CharSequence... charSequences){
    webElement.sendKeys(charSequences);
  }

  public void clearAndType(CharSequence... charSequences) {
    webElement.clear();
    webElement.sendKeys(charSequences);
  }

  public void typeTrimmedMessage(CharSequence... charSequences) {
    String trimmedMessage = Stream.of(charSequences).collect(Collectors.joining("")).trim();
    webElement.sendKeys(trimmedMessage);
  }
}
