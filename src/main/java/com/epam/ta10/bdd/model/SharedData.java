package com.epam.ta10.bdd.model;

public class SharedData {

  private int draftsNumber;

  public int getDraftsNumber() {
    return draftsNumber;
  }

  public void setDraftsNumber(int draftsNumber) {
    this.draftsNumber = draftsNumber;
  }
}
