package com.epam.ta10.bdd.model;

import java.util.Objects;

public class Message {

  private String recipient;
  private String theme;
  private String text;

  public Message(String recipient, String theme, String text) {
    this.recipient = recipient;
    this.theme = theme;
    this.text = text;
  }

  public String getRecipient() {
    return recipient;
  }

  public String getTheme() {
    return theme;
  }

  public String getText() {
    return text;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Message message = (Message) o;
    return Objects.equals(recipient, message.recipient) &&
        Objects.equals(theme.trim(), message.theme.trim()) &&
        Objects.equals(text.trim(), message.text.trim());
  }

  @Override
  public int hashCode() {
    return Objects.hash(recipient, theme, text);
  }
}
