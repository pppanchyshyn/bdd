package com.epam.ta10.bdd.bo;

import com.epam.ta10.bdd.model.User;
import com.epam.ta10.bdd.po.GmailLoginPage;
import com.epam.ta10.bdd.po.GmailPasswordPage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class GmailAuthenticationBO {

  private GmailLoginPage gmailLoginPage;
  private GmailPasswordPage gmailPasswordPage;
  private static Logger logger = LogManager.getLogger(GmailAuthenticationBO.class);

  public GmailAuthenticationBO() {
    gmailLoginPage = new GmailLoginPage();
    gmailPasswordPage = new GmailPasswordPage();
  }

  public void login(User user) {
    navigate();
    enterLoginAndPressButtonNext(user.getLogin());
    enterPasswordAndPressButtonNext(user.getPassword());
  }

  private void navigate(){
    gmailLoginPage.navigate();
  }

  private void enterLoginAndPressButtonNext(String login) {
    logger.info("Entering user login...");
    gmailLoginPage.getLogin().clearAndType(login);
    logger.info("Pressing button next..");
    gmailLoginPage.getNext().clickAfterButtonBecomeClickable();
  }

  private void enterPasswordAndPressButtonNext(String password) {
    logger.info("Entering user password...");
    gmailPasswordPage.getPassword().clearAndType(password);
    logger.info("Pressing button next...");
    gmailPasswordPage.getNext().clickAfterButtonBecomeClickable();
  }
}
