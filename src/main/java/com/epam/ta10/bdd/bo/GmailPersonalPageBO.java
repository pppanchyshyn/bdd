package com.epam.ta10.bdd.bo;

import com.epam.ta10.bdd.model.Message;
import com.epam.ta10.bdd.po.GmailPersonalPage;
import com.epam.ta10.bdd.utils.WaiterProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class GmailPersonalPageBO {

  private GmailPersonalPage gmailPersonalPage;
  private static Logger logger = LogManager.getLogger(GmailPersonalPageBO.class);

  public GmailPersonalPageBO() {
    gmailPersonalPage = new GmailPersonalPage();
  }

  public void createMessage(Message message) {
    logger.info("Pressing button to create new message...");
    gmailPersonalPage.getCreateMessage().click();
    logger.info("Entering message recipient...");
    WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getMessageRecipient())
        .sendKeys(message.getRecipient());
    logger.info("Entering message theme...");
    gmailPersonalPage.getMessageTheme().typeTrimmedMessage(message.getTheme());
    logger.info("Entering message text...");
    gmailPersonalPage.getMessageText().typeTrimmedMessage(message.getText());
  }

  public void saveMessageAsDraft() {
    logger.info("Saving message as draft...");
   WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getSaveAsDraft()).click();
  }

  public int getDraftsNumber() {
    logger.info("Getting number of drafts...");
    int draftsNumber = 0;
    WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getDrafts());
    if (isElementExist(gmailPersonalPage.getDraftsNumber())) {
      draftsNumber = Integer.valueOf(gmailPersonalPage.getDraftsNumber().getText());
    }
    logger.info(String.format("Number of drafts - [%d]", draftsNumber));
    return draftsNumber;
  }

  public void moveToDrafts() {
    logger.info("Moving to drafts...");
    WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getDrafts()).click();
  }

  public void moveToSentMessages() {
    logger.info("Moving to sent messages...");
    WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getSentMessages()).click();
  }

  public void sendMessageFromDrafts() {
    logger.info("Opening the newest draft...");
    WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getNewestDraft()).click();
    logger.info("Sending message...");
    gmailPersonalPage.getSendMessage().clickAfterButtonBecomeClickable();
  }

  public boolean isLastSentMessageEqualsToCreatedMessage(Message message) {
    logger.info("Comparing last sent message with created message...");
    return message.equals(getLastSentMessage());
  }

  private Message getLastSentMessage() {
    WaiterProvider.waitElementToBeClickable(gmailPersonalPage.getLastSentMessageRecipient());
    logger.info("Getting message recipient from last sent message...");
    String messageRecipient = gmailPersonalPage.getLastSentMessageRecipient().getAttribute("email");
    logger.info("Getting message theme from last sent message...");
    String messageTheme = gmailPersonalPage.getLastSentMessageTheme().getText();
    logger.info("Getting message text from last sent message...");
    String messageText = gmailPersonalPage.getLastSentMessageText()
        .cutInnerLabelText(gmailPersonalPage.getLastSentMessageTextInnerTechnicalLabel());
    return new Message(messageRecipient, messageTheme, messageText);
  }

  private boolean isElementExist(WebElement webElement) {
    boolean isExist;
    try {
      webElement.getTagName();
      isExist = true;
    } catch (NoSuchElementException e) {
      isExist = false;
    }
    return isExist;
  }
}
