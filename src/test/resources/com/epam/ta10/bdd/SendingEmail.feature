Feature: Sending email

  Scenario Outline: Send email from drafts
    When User login to gmail with <login> and <password>
    And System get actual number of drafts
    And User write new message
    And User save it as drafts
    And User navigate to drafts
    Then System compare drafts number before and after creating new one
    And User send created message from drafts
    And User move to sent messages
    And System compare last sent message with message that user created before

    Examples:
      | login                  | password     |
      | pashkopanch1@gmail.com | TestPassword |
      | pashkopanch4@gmail.com | TestPassword |
      | pashkopanch5@gmail.com | TestPassword |
      | pashkopanch7@gmail.com | TestPassword |