package com.epam.ta10.bdd;

import com.epam.ta10.bdd.model.Message;
import com.epam.ta10.bdd.model.User;
import com.epam.ta10.bdd.utils.PropertyProvider;

public class Const {

  private final static String MESSAGE_RECIPIENT = "pashkopanch@gmail.com";
  private final static String MESSAGE_THEME = "Hello";
  private final static String MESSAGE_TEXT = "This is test message";
  public final static Message MESSAGE = new Message(MESSAGE_RECIPIENT, MESSAGE_THEME, MESSAGE_TEXT);
  public final static User USER1 = new User(PropertyProvider.PROPERTIES.getProperty("login1"),
      PropertyProvider.PROPERTIES.getProperty("password1"));
  public final static User USER2 = new User(PropertyProvider.PROPERTIES.getProperty("login2"),
      PropertyProvider.PROPERTIES.getProperty("password2"));
  public final static User USER3 = new User(PropertyProvider.PROPERTIES.getProperty("login3"),
      PropertyProvider.PROPERTIES.getProperty("password3"));
  public final static User USER4 = new User(PropertyProvider.PROPERTIES.getProperty("login4"),
      PropertyProvider.PROPERTIES.getProperty("password4"));
  public final static User USER5 = new User(PropertyProvider.PROPERTIES.getProperty("login5"),
      PropertyProvider.PROPERTIES.getProperty("password5"));

  private Const() {
  }
}
