package com.epam.ta10.bdd;

import com.epam.ta10.bdd.bo.GmailAuthenticationBO;
import com.epam.ta10.bdd.bo.GmailPersonalPageBO;
import com.epam.ta10.bdd.model.SharedData;
import com.epam.ta10.bdd.model.User;
import com.epam.ta10.bdd.utils.DriverProvider;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.Assert;

public class Stepdefs {

  private SharedData sharedData;
  private GmailPersonalPageBO gmailPersonalPageBO;

  @Before
  public void setUp() {
    System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
    sharedData = new SharedData();
    gmailPersonalPageBO = new GmailPersonalPageBO();
  }

  @When("^User login to gmail with ([^\"]*) and ([^\"]*)$")
  public void login(String login, String password) {
    GmailAuthenticationBO gmailAuthenticationBO = new GmailAuthenticationBO();
    gmailAuthenticationBO.login(new User(login, password));
  }

  @And("^System get actual number of drafts$")
  public void getDraftsNumber() {
    sharedData.setDraftsNumber(gmailPersonalPageBO.getDraftsNumber());
  }

  @And("^User write new message$")
  public void createMessage() {
    gmailPersonalPageBO.createMessage(Const.MESSAGE);
  }

  @And("^User save it as drafts$")
  public void saveMessageAsDraft() {
    gmailPersonalPageBO.saveMessageAsDraft();
  }

  @And("^User navigate to drafts$")
  public void navigateToDrafts() {
    gmailPersonalPageBO.moveToDrafts();
  }

  @Then("^System compare drafts number before and after creating new one$")
  public void compareDraftsNumberBeforeAndAfterCreatingNewOne() {
    Assert.assertEquals(gmailPersonalPageBO.getDraftsNumber(),
        sharedData.getDraftsNumber() + 1,
        "The number of drafts after creation of a new one should be one more than before");
  }

  @And("^User send created message from drafts$")
  public void sendMessageFromDrafts() {
    gmailPersonalPageBO.sendMessageFromDrafts();
  }

  @And("^User move to sent messages$")
  public void navigateToSentMessages() {
    gmailPersonalPageBO.moveToSentMessages();
  }

  @And("^System compare last sent message with message that user created before$")
  public void compareLastSentMessageWithMessageCreatedBefore() {
    Assert.assertTrue(gmailPersonalPageBO.isLastSentMessageEqualsToCreatedMessage(Const.MESSAGE),
        "Last sent message is not equals to created message");
  }

  @After
  public void closeWebDriver() {
    DriverProvider.close();
  }

}
