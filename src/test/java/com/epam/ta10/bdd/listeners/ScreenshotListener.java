package com.epam.ta10.bdd.listeners;

import com.epam.ta10.bdd.utils.DriverProvider;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

public class ScreenshotListener extends TestListenerAdapter {

  @Override
  public void onTestStart(ITestResult result) {
    Reporter.log(String
        .format("Starts %s on thread %s", getCurrentTime(), Thread.currentThread().getId()));
  }

  @Override
  public void onTestSuccess(ITestResult result) {
    Reporter.log(String.format("Ends %s", getCurrentTime()));
  }

  @Override
  public void onTestFailure(ITestResult result) {
    Reporter.log(String.format("Ends %s", getCurrentTime()));
    Calendar calendar = Calendar.getInstance();
    String dateFormat = new SimpleDateFormat("dd_MM_yyyy_hh_mm_ss").format(calendar.getTime());
    String methodName = result.getName();
    File scrFile = ((TakesScreenshot) DriverProvider.getDriver())
        .getScreenshotAs(OutputType.FILE);
    String fileParentDirectory = "failure_screenshots";
    String reportDirectory = String.format("%s/target/surefire-reports",
        new File(System.getProperty("user.dir")));
    String fileName = String
        .format("%s_%s_%s.png", methodName, dateFormat, Thread.currentThread().getName());
    File destFile = new File(
        String.format("%s/%s/%s", reportDirectory, fileParentDirectory, fileName));
    try {
      FileUtils.copyFile(scrFile, destFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
    Reporter.log(String.format(
        "<br><a href='%s/%s'> <img src='%s/%s' height='100' width='100'/> </a>",
        fileParentDirectory, fileName, fileParentDirectory, fileName));
  }

  private String getCurrentTime() {
    Calendar calendar = Calendar.getInstance();
    return new SimpleDateFormat("dd-MM-yyyy hh:mm:ss").format(calendar.getTime());
  }
}
